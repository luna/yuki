require "logger"
require "sqlite3"


def open_db(log)
  db = DB.open "sqlite3://data.db"
  puts db

  db.exec("create table if not exists comics (
    number int primary key,
    title text,
    url text,
    image_url text,
    transcript text
  )")

  total = db.query_one("select count(*) from comics", as: { Int32 })

  log.info("total comics: #{total}")
  puts total

  return db
end

def new_logger
  Logger.new(STDOUT, level: Logger::INFO)
end

def fetch_all_comics(db, log = nil)
  log = log || new_logger

  comics = db.query_all(
    "select number, title, url, image_url, transcript from comics
    order by number asc",
    as: { Int32, String, String, String, String }
  )

  res = [] of Comic

  # make a Comic instance for each
  comics.each do |comic_tup|
    comic = Comic.new(comic_tup[0], comic_tup[1],
                      comic_tup[2], comic_tup[3], comic_tup[4])
    res.insert(0, comic)
  end

  return res
end

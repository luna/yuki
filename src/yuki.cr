require "kemal"

require "./comic"
require "./comic_db"
require "./const"
require "./util"
require "./sync"

if ARGV.includes?("--debug")
  level = Logger::DEBUG
else
  level = Logger::DEBUG
end

log = Logger.new(STDOUT, level: level)

db = open_db log

get "/" do
  "hewwo (◕‿◕✿)"
end


def calculate_score(term : String, comic : Comic,
                    debug : Bool = false) : Int32
  # first score for the given comic is how many parts of the title
  # match with the given term.
  score = TITLE_WORD_WEIGHT * \
    simple_match(term, comic.title_fields)

  if debug
    puts term
    puts comic.title_fields
    puts comic.@title.downcase

    puts "post-title word weight #{score}"
  end

  # then we add that with the amount of times term appears on the title's
  # text (raw text, not "parts of title", from the first score)
  score += TITLE_INTEXT_WEIGHT * \
    comic.@title.downcase.scan(term).size

  if debug
    puts "post-title intext weight #{score}"
  end

  # match the given term against transcript, text on the comic
  # (with word splitting) and text on the comic without word splitting.
  score += TRANSCRIPT_WEIGHT * \
    comic.@transcript.downcase.scan(term).size

  if debug
    puts "post-transcript weight #{score}"
  end

  return score
end


post "/search" do |env|
  terms = env.params.json["search"].as(String)
  res = [] of Comic

  terms.split " " do |term|
    term = term.downcase
    scores = [] of ScoreRecord

    comics = fetch_all_comics db

    comics.each_with_index do |comic, i|
      if SKIPPABLE.includes?(comic.@number)
        next
      end

      if res.size > 15
        break
      end

      idx = scores.size
      scores.insert(idx, ScoreRecord.new(i))

      scores[idx].score = calculate_score(term, comic)

      # sadly, i can't extract full words from explainxkcd.
      # mediawiki api sucks
      #scores[i].score += TEXT_WORD_WEIGHT * simple_match(
      #  term, comic.@text.downcase.split(" "))
      #scores[i].score = TEXT_WEIGHT * comic.@text.downcase.count(term)
    end

    # sort by score (an int)
    sorted = scores.sort do |record_a, record_b|
      record_a.score <=> record_b.score
    end

    # only extract max 15 comits on the result
    sorted = sorted.reverse[0, 15]

    # NOTE: this is debug only stuff

    #puts sorted.map { |entry|
    #  comic = comics[entry.@index]
    #  # recalc with debug=true to help me
    #  recalc = calculate_score term, comic, true
    #  {comic.number, comic.title, entry.@score, recalc}
    #}

    sorted.each_with_index do |entry, i|
      if entry.@score > 0
        res.insert(res.size, comics[entry.@index])
      end
    end

  end

  env.response.content_type = "application/json"

  {
    success: true,
    results: res
  }.to_json
end

begin
  spawn do
    comic_db_sync db, log
  end
  log.info "started background sync"

  Kemal.run
ensure
  log.info "closing db"
  db.close
end

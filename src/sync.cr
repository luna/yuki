require "http/client"
require "xml"

require "./comic"
require "./comic_db"
require "./const"
require "./semaphore"

def fetch_comic(num, db, log) : Comic?
  existing_title = db.query_one?(
    "select title from comics where number = ?", num, as: { String })

  if !existing_title.nil?
    log.debug("already loaded #{num}, skipping")
    return
  end

  comic = Comic.new(0, "", "", "", "")

  resp = HTTP::Client.get("https://xkcd.com/#{num}/info.0.json")
  if resp.status_code != 200
    log.warn("Failed to fetch comic #{num}: non-200 status")
    return
  end

  page_data = JSON.parse(resp.body)

  comic.url = "https://xkcd.com/#{num}"
  comic.number = num
  comic.title = page_data["safe_title"].as_s
  comic.transcript = page_data["transcript"].as_s
  comic.image_url = page_data["img"].as_s

  return comic
end

def insert_comic(db, log, comic : Comic)
  log.debug "insert comic #{comic.number}"

  db.exec "insert into comics
    (number, title, url, image_url, transcript)
  values
    (?, ?, ?, ?, ?)",
    comic.number, comic.title, comic.url, comic.image_url, comic.transcript
end

def comic_db_sync(db, log = nil)
  log = log || new_logger
  log.info("starting comic db sync, may take time")

  resp = HTTP::Client.get(XKCD_BASE_URL)
  if resp.status_code != 200
    log.fatal "failed to get xkcd comic list"
    return
  end

  log.info "got #{resp.body.size} bytes"

  latest_data = JSON.parse(resp.body)

  semaphore = Semaphore.new 10
  comics_done = 0
  latest_page = latest_data["num"].as_i

  puts "latest comic: #{latest_page}"

  (1..latest_page).each do |num|
    spawn do
      begin
        semaphore.acquire
        comic = fetch_comic(num, db, log)

        if comic.nil?
          next
        end

        # insert to db
        insert_comic db, log, comic
        comics_done += 1
      ensure
        semaphore.release
      end
    end
  end

  semaphore.wait
  puts "all done, #{comics_done} comics on sync"

end

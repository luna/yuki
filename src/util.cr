def simple_match(string : String, list : Array(String))
  count = 0

  list.each do |word|
    if word == string
      count += 1
    end
  end

  return count
end

require "fiber"
require "./semaphore"

s = Semaphore.new 2

(0..5).each do |i|
  spawn do
    begin
      s.acquire
      puts i
      sleep 1.seconds
    ensure
      s.release
    end
  end
end

s.wait

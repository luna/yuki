class Comic
  property number : Int32
  property url : String
  property title : String
  property transcript : String
  property image_url : String

  def initialize(@number, @title, @url, @image_url, @transcript)
  end

  def title_fields
    self.@title.split " "
  end

  def to_json(json)
    {
      number: self.@number,
      title: self.@title,
      url: self.@url,
      image_url: self.@image_url,
      transcript: self.@transcript
    }.to_json json
  end
end

class ScoreRecord
  property score : Int32

  def initialize(@index : Int32)
    @score = 0
  end
end

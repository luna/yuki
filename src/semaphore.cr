class Semaphore
  def initialize(total : Int32)
    @chan = Channel(Nil).new total
  end

  def acquire
    @chan.send(nil)
  end

  def release
    @chan.receive()
  end

  def wait
    Fiber.yield
    while true
      if @chan.empty?
        break
      end

      Fiber.yield
    end
  end

end

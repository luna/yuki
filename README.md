# yuki

[relevant-xkcd](https://github.com/adtac/relevant-xkcd) but crystal, only
the backend.

## Installation

TODO: Write installation instructions here

## Usage

TODO: Write usage instructions here

## Development

TODO: Write development instructions here

## Contributing

1. Fork it (<https://gitlab.com/luna/yuki/forks/new>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Merge Request

## Contributors

- [Luna](https://gitlab.com/luna) - creator and maintainer
